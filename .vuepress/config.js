const path = require('path');

module.exports = {
    title: 'Информационная система',
    base: '/information-system-2018/',
    dest: 'public',
    description: "ИПИ - информационная поддержка процессов жизненного цикла изделий",
    components: [
        {
            path: '../components/TestContainer.vue',
            name: 'test-container',
        }
    ],
    themeConfig: {
        nav: [
            {
                text: 'Лекции',
                link: '/lections/',
            },
            {
                text: 'Практика',
                link: '/practice/',
            },
            {
                text: 'Тестирование',
                link: '/knowledge-check/',
            },
        ],
        sidebar: {
            '/lections/': [
                {
                    title: 'Лекции',
                    collapsable: false,
                    children: [
                        '1',
                        '2',
                        '3',
                        '4',
                        '5',
                        '6',
                        '7',
                        '8',
                        '9',
                        '10',
                        '11',
                        '12',
                        '13',
                        '14',
                        '15',
                        '16',
                        '17',
                    ],
                },
            ],
            '/practice/': [
                {
                    title: 'Практика',
                    collapsable: false,
                    children: [
                        '1',
                        '2',
                        '3',
                    ],
                },
            ],
            '/knowledge-check/': [
                {
                    title: 'Проверка знаний',
                    collapsable: false,
                    children: [
                        '1',
                        '2',
                        '3',
                        '4',
                        '5',
                        '6',
                        '7',
                        '8',
                        '9', 
                        '10',
                        '11',
                        '12',
                        '13',
                        '14',
                        '15',
                        '16',
                        '17',
                    ],
                },
            ],
        },
    },
}