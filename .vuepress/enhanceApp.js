export default ({ router }) => {
    router.addRoutes([
        {
            path: '/lections/', 
            redirect: '/lections/1.html',
        },
        {
            path: '/practice/', 
            redirect: '/practice/1.html',
        },
        {
            path: '/knowledge-check/', 
            redirect: '/knowledge-check/1.html',
        },
    ]);
}