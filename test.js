let fs = require('fs');
let path = require('path');

let practiceName = 'lab2';
let folders = fs.readdirSync(path.resolve(`./practice/practice/${practiceName}`));


folders.forEach(fileName => fs.renameSync(path.resolve(`./practice/practice/${practiceName}/${fileName}`), path.resolve(`./practice/practice/${practiceName}/${fileName.split('.')[0]}.png`)));
// fs.renameSync()